#!/usr/bin/env python3

# Take first header, minus otherinfo, and merge with subset of second header( from format till the end)
#For example,
# Chr Start End Ref Alt Func.refGene Gene.refGene GeneDetail.refGene ExonicFunc.refGene ... Otherinfo
# chr   start   end ref alt                                           format  TCGA-CV-5430-11B-01D-1683-08
# becomes
# Chr Start End Ref Alt Func.refGene Gene.refGene GeneDetail.refGene ExonicFunc.refGene ... format TCGA-CV-5430-11B-01D-1683-08

import sys

with open (sys.argv[1], "r") as inputfile, open (sys.argv[2], "w") as outputfile:
    header1 = inputfile.readline().rstrip().split("\t")
    header2 = inputfile.readline().rstrip().split("\t")
    header1.pop() # remove the last element, which is "Otherinfo"
    
    # combine header1 and subset of header2
    header1.extend(header2[header2.index("format"):])

    # first line of annotation
    line = inputfile.readline().rstrip().split("\t")

    if len(line) != len(header1):
        raise UserWarning("header length doesn't match data length")

    outputfile.write("\t".join(header1) + "\n")
    outputfile.write("\t".join(line) + "\n")

    # print the rest of the file
    for line in inputfile:
        outputfile.write(line)    
