#NOTE set genome reference, python2 path, sourcedata
# hg38
reference_fasta : $TEAM_ROOT/reference/genomics/Hsap/GRCh38.d1.vd1/GRCh38.d1.vd1.fa
mastervcf       : $TEAM_ROOT/reference/genomics/Hsap/GRCh38.d1.vd1/1kg/ez3/1kg-exome3.hg38.vcf.updated_ref
1kg_phase_dir   : $TEAM_ROOT/reference/genomics/Hsap/GRCh38.d1.vd1/1kg/phasing

# hg19 
# reference for Maitra barcoded seq
#reference_fasta : $TEAM_ROOT/reference/genomics/Hsap/maitra_hg19/hg19.fasta
#mastervcf       : $TEAM_ROOT/reference/genomics/1kg/1kg-exome.HLA_filtered.vcf
#1kg_phase_dir   : $TEAM_ROOT/reference/genomics/1kg-phasing/

# Broads GRCh37 - this reference doesn't have chr prefix, you'll need to remove chr prefix in gatk genotype calling step in phase-sample.template 
#reference_fasta : $TEAM_ROOT/reference/genomics/Hsap/Broad-GRCh37/Homo_sapiens_assembly19.fasta
#mastervcf       : $TEAM_ROOT/reference/genomics/1kg/ez3/1kg-exome3.HLA_filtered.no-chr.vcf
#1kg_phase_dir   : $TEAM_ROOT/reference/genomics/1kg-phasing/

#sourcedata	    : $REFLIB/TCGA_restricted/Level1/download/LUSC/artifacts/allbams 
sourcedata	    : $PWD

python2                 : /risapps/rhel7/python/2.7.15/bin/python
#python2                 : /risapps/rhel6/python/2.7.9-2/anaconda/bin/python
python                  : python
pythonpath              : $TEAM_ROOT/lib/python/zip/labtools-1.6.6.zip
python3_setup           : source activate python3.6
python3_path_setup      : export PYTHONPATH=$TEAM_ROOT/lib/python/zip/labtools-3.2.5.zip

#haplohseq param
missing_gt_mode                         : dot
haplohseq_em_iterations                 : 25
haplohseq_end_param_event               : 0.9
haplohseq_event_mb                      : 20
haplohseq_event_prevalence              : 0.05
haplohseq_genome_mb                     : 3156
haplohseq_initial_param_event           : 0.6
haplohseq_num_param_event_starts        : 10
haplohseq_num_states                    : 2
haplohseq_plot_states                   : S1
haplohseq_plot_states_colors            : blue
# phase_concordance or raf_deviation (should be read_counts)
haplohseq_obs_type			            : phase_concordance
graphics        : $TEAM_ROOT/3rdparty/no-arch/graphics-r1796/graphics
#bedtools        : $TEAM_ROOT/3rdparty/$OS/bins/BEDTools-Version-2.16.2-bin/bedtools
gatk_jar	    : $TEAM_ROOT/3rdparty/no-arch/java/GenomeAnalysisTK-3.6/GenomeAnalysisTK.jar
genomics        : $TEAM_ROOT/reference/genomics
java8           : /risapps/noarch/jdk/jdk1.8.0_45/bin/java
1kg_phase_version : v2.20101123
haplohseq       : $TEAM_ROOT/3rdparty/no-arch/haplohseq_tools/haplohseq_gnu_linux_cluster
mach            : $TEAM_ROOT/3rdparty/no-arch/mach.1.0.18/executables/mach1
vcftoped        : $TEAM_ROOT/3rdparty/no-arch/haplohseq_tools/vcfToPed.py
get1kgsnphaps   : $TEAM_ROOT/3rdparty/no-arch/haplohseq_tools/get1kgSnpHaps.py
haplohseq_tools : $TEAM_ROOT/3rdparty/no-arch/haplohseq_tools
ped_min_depth   : 10
ped_sample_sex  : M
num_1kg_haplotypes : 600
mach_rounds     : 30
mach_states     : 50

# for imputed vcf
imputed_marker_reference        : $TEAM_ROOT/reference/genomics/Hsap/TCGA_imputation/hg38
imputed_vcf_suffix              : .dose.vcf
imputed_vcf_prefix              : /rsrch3/scratch/epi/scheet/PROJECTS/TCGA/HapRNA/extract-haplotypes/phased-vcf/R2.grt.0.3.
vcf_header                      : /rsrch3/scratch/epi/scheet/PROJECTS/TCGA/HapRNA/extract-haplotypes/vcf.header
create-haps-script              : $TEAM_ROOT/3rdparty/no-arch/haplohseq_tools/create-haps-file.py
liftover_imputed_markers_path   : /rsrch3/scratch/epi/scheet/PROJECTS/TCGA/HapRNA/extract-haplotypes/final-markers/
liftover_imputed_markers_suffix : .no.conflicts.tab

# misc.
# simple_phaser   : $SYQADA/resources/workflows/tutorial/HAPLOHSEQ/example_files/scripts/simple_phaser.py
# 1kg_ldmap       : $TEAM_ROOT/reference/pairwise-phase/ldmaps/ldmap_hg38/1kg.ez3.hg38.{chromosome}.ldmap
