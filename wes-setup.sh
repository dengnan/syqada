#!/usr/bin/env bash

# probably outdated...

# setup all the syqada pipeline folders typically needed for WES analysis in your current directory
# it requires a tumor_normal file, and some helper shell scripts in ~kchang3/bin/
# usage: sh wes_setup.sh [tumor_normal_file]

pairs_file=$PWD/$1
main_dir=$PWD

function make_dir () {
    analysis_dir=$1
    # path to my syqada pipeline directories
    syqada_dir=$HOME/software/syqada
    syqada_template_dir=$syqada_dir/$2
    syqada_config_dir=$syqada_dir/protocols
    syqada_config_prefix=$3

    # generate control folder, cp config/protocol and make samples files. link templates
    mkdir $analysis_dir && mkdir $analysis_dir/control && \
    cd $analysis_dir/control && cp -av $syqada_config_dir/$syqada_config_prefix.* .  && \
    cp -av $pairs_file $syqada_config_prefix.tumor_normal && \
    prep-control.sh $syqada_config_prefix && \
    cd .. && ln -s $syqada_template_dir/* . && \
    cd $main_dir
    
    #if mutect2, get annotation templates too 
    if [ "$analysis_dir" == "somatic-mutect2" ]; then
        cd $analysis_dir && ln -s $syqada_dir/annotations/* . && cd $main_dir
    fi
}

# setup analysis folders
make_dir alignment wes wes
make_dir qc seq-qc qc
make_dir coverage coverage coverage
make_dir somatic-mutect2 somaticvariants mutect2
make_dir neoantigen neoantigen pvacseq
#NOTE: need to prepare samples file manually to include all normal samples 
make_dir germline-gatk germline-variants haplotypecaller
