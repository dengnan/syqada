# SyQADA workflows

## Project setup
* git clone or download the files to your folder
* cd your-analysis-folder && mkdir workflows && cp X/*.templates workflows
* cp X.protocol X.config control



| Type | Pipeline | source |
| ------ | ------ | ------ |
| SNV/InDEL | Mutect2 tumor vs normal with annovar |[templates](/somaticvariants), [protocol](/protocol/mutect2.protocol), [config](/protocol/mutect2.config)    |
| | Mutect2 with panel of normal (mice genome) | [templates](/somaticvariants-mm10), [protocol](/protocols/mutect2pon-mm10.protocol), [config](/protocols/mutect2pon-mm10.config) | 
| | Merging Mutect2 and MsMutect VCF|[templates](/somaticvariants/merge-somatic.template), [protocol](/protocols/merge-somatic.protocol), [config](/protocol/merge-somatic.config)  |
| | Germline variants with GATK Haplotyper | [templates](/germline-variants/), [protocol](/protocols/haplotypecaller.protocol), [config](/protocols/haplotypecaller.config) | 
| CNV | Germline CNV with XHMM | [templates](/cnv/xhmm), [protocol](/protocols/xhmm.protocol),[config](/protocols/xhmm.config) |
| | Germline CNV with Conifer | [templates](/cnv/conifer), [protocol](/protocols/conifer.protocol), [config](/protocols/conifer.config) |
| | hapLOHseq | [templates](/haplohseq), [protocol](/protocols/haplohseq.protocol), [config](/protocols/haplohseq.config) |
| RNA Expression | RSEM (bowtie alignment) | [templates](/rnaseq/rsem-bowtie-expression.template), [protocol](/protocols/rsem-bowtie.protocol), [config](/protocols/rsem-bowtie.config) |
| | RSEM (STAR alignment) | [templates](/rnaseq/rsem-star-expression.template), [protocol](/protocols/rsem-star.protocol), [config](/protocols/rsem-star.config) |
| microRNA Expression | STAR and featureCounts | [templates](/mirnaseq/), [protocol](/protocols/star-count-mm10.protocol), [config](/protocols/star-count-mm10.config) | 
| Alignment | WES (BWA) + coverage stats | [templates](/wes/), [protocol](/protocol/wes.protocol), [config](/protocol/wes.config)
|  | RNAseq (STAR) | [templates](/rnaseq/star-align.template/), [protocol](/protocols/star-align.protocol), [config](/protocols/star-align.config)
| Annotations | annovar | [templates](/annotations/), [protocol](/protocols/annovar.protocol), [config](/protocols/annovar.config) | 
| Neoantigen | HLA typing (PHLAT) | [templates](/neoantigen/phlat.template), [protocol](/protocols/phlat.protocol), [config](/protocols/phlat.config) | 
| | Peptide Binding Prediction (PVACSeq) | [templates](/neoantigen/), [protocol](/protocols/pvacseq.protocol), [config](/protocols/pvacseq.config) | 
| Data QC | tumor-normal pair swap/contamination | [templates](/seq-qc/contest.template), [protocol](/protocols/contest.protocol), [config](/protocols/contest.config) | 
|  | trim fastq + QC | [templates](/wes/trim-fastq.template), [protocol](/protocols/trimfq.protocol), [config](/protocols/trimfq.config)
| VCF utils | liftover/split VCF | [templates](/vcf-util/) | 